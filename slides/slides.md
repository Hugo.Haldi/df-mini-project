---
theme: seriph
background: https://source.unsplash.com/collection/94734566/1920x1080
class: text-center
highlighter: shiki
lineNumbers: false
info: |
  ## Presentation on WavMark
  A DNN-based Audio Watermarking Tool
drawings:
  persist: false
transition: slide-left
title: "WavMark: AI-based Audio Watermarking Tool"
mdc: true
---

# WavMark: AI-based Audio Watermarking Tool

DNN-based Audio Watermarking Tool

Paper by Guangyu Chen, Yu Wu, Shujie Liu, Tao Liu, Xiaoyong Du†, Furu Wei

---

## What is Audio Watermarking?

- Audio Watermarking is a technique to embed information into audio signals.
- It is used for various purposes such as:
  - Copy protection
  - Content identification
  - Broadcast monitoring
  - etc.
- Use of DNNs: auto encoding strategies

---

## Why is Audio Watermarking important?

- Detects voice cloning
- Assure authenticity of a voice recording
- Detects tampering of audio files

---

## What is WavMark?

- WavMark is a DNN-based Audio Watermarking Tool.
- It is developed by Microsoft Research Asia and Renmin University of China.
- Each second of audio can embed 32 bits of information.

![WavMark](/images/structure_v2.png)

---
layout: default
---

## How does WavMark work?

- Use the frequency domain of the audio signal for higher robustness.
- Architecture:
  - invertible Encoder/Decoder (INN)
  - shift module
  - attack simulator

---

![Architecture](/images/architecture.png)

---

## Message representation

Short-time Fourier transform (STFT):

$$x_{spec} = \Gamma_{STFT}(x_{wave})$$

Watermark message is represented as a binary vector of length K:

$$m_{vec} \in \{0, 1\}^K \\
m_{spec} = \Gamma_{STFT}\left(\Gamma_{FC}\left(m_{vec}\right)\right)$$

where $\Gamma_{FC} \in \mathbb{R}^{L \times K}$ is a linear layer used for dimensionality transformation.

---

## Attack simulator

- Random noise: average SNR of 34.5 dB
- Sample suppression: randomly suppress 0.1% of the samples
- Low-pass filter: 5 kHz cutoff frequency
- Median filter: kernel size of 3
- Re-Sampling: $2 \times$ or $0.5 \times$ sampling rate, followed by re-conversion to original frequency
- Amplitude Scaling: Reduce the amplitude to 90%
- Lossy Compression: MP3 compression with 64 kbps bitrate
- Quantization: $2^9$ quantization levels
- Echo addition: attenuation by $0.3$ audio volume, delay of $100$ ms
- Time stretch: $1.1$ or $0.9$ times the original speed

---

## Results

![Results](/images/results.png)

Comparison with existing DNN-based methods. MEAN: the average BER value across all attack scenarios (including the 'No Attack' setting).

---
layout: end
---

## References

- [WavMark: Watermarking for Audio Generation](https://arxiv.org/abs/2308.12770)

---
title: "Digital Forensics Mini Project"
subtitle: "WavMark: Watermarking for Audio Generation"
date: \today
author: "Haldi Hugo"
geometry: margin=3cm
---

# Introduction

With the rise of deep learning, it has become easier to generate fake audio and video. This is a problem for digital forensics, as it is now easier to create fake evidence. This is why watermarking is important. It allows to detect if a file has been tampered with.

In this project, we will use WavMark, a DNN-based Audio Watermarking Tool. It is developed by Microsoft Research Asia and Renmin University of China. Each second of audio can embed 32 bits of information. This tool is based on the paper [WavMark: Watermarking for Audio Generation](https://arxiv.org/pdf/2308.12770.pdf).

# How does WavMark work?

WavMark uses the frequency domain of the audio signal for higher robustness. The architecture is composed of an invertible Encoder/Decoder (INN), a shift module and an attack simulator.

This model has been trained using the [LibriSpeech](https://www.openslr.org/12), [Common Voice](https://commonvoice.mozilla.org/en), [Audio Set](http://research.google.com/audioset/) and [Free Music Archive](https://freemusicarchive.org/) datasets.

The audio stream is first converted to the frequency domain using a Short-Time Fourier Transform (STFT). The resulting spectrogram is then fed to the INN. The INN is a reversible neural network that can be used for watermarking. The INN is composed of a series of invertible blocks. The input and output dimensions of each block are the same.

$$x_{spec} = \Gamma_{STFT}\left(x_{wave}\right)$$

The message that needs to be embedded is a binary vector of length $K$. It is first fed to a linear layer. The result is then fed to another STFT layer.

$$m_{vec} \in \{0, 1\}^K \\
m_{spec} = \Gamma_{STFT}\left(\Gamma_{FC}\left(m_{vec}\right)\right)$$

This way, the message is represented in the frequency domain. The message is then embedded in the spectrogram using the INN. The resulting spectrogram is then converted back to the time domain using an inverse STFT.

# Performance Evaluation

In the ensuing experiments, we employed audio files from MusicGen and LibriSpeech, embedding a binary message within them and evaluating metrics across various attack scenarios. The message itself is a randomly generated binary vector of length $K=32$.

All attack implementations are detailed in the `attacks.ipynb` Jupyter Notebook.

## Baseline: No Attack

Under pristine conditions without any attack, the message is recoverable with 100% accuracy.

## Random Noise Attack

Introducing random noise to the audio signal proved to be an effective attack against WavMark. Even with a high Signal-to-Noise Ratio (SNR), recovery accuracy declines beyond a noise standard deviation (std) of 0.004.

## Sample Suppression Attack

Random suppression of audio samples presents a variable challenge. While recovery remains at 100% accuracy for up to 2% of samples suppressed, inconsistency arises, making recovery unpredictable when exceeding 2.5% suppression.

## Low-pass Filter Attack

Employing a low-pass filter, with a cutoff frequency determined by the original 16 kHz sampling rate, demonstrated that recovery accuracy stays at 100% for a cutoff frequency of 3 kHz. However, accuracy diminishes when the cutoff frequency falls below 2.5 kHz.

## Median Filter Attack

Similar to sample suppression, the median filter attack modifies sample values. With a kernel size of 3, recovery accuracy remains at 100%, but with larger kernel sizes (e.g., 7), complete recovery becomes unattainable.

## Amplitude Scaling Attack

Scaling the amplitude of the audio signal impacts recovery. With a scaling factor of 0.02, recovery is feasible, but beyond factors of 0.015 or 0.01, successful recovery becomes unattainable.

## Lossy Compression Attack

Reducing the audio file size through lossy compression (e.g., MP3) exhibited a threshold effect. Recovery accuracy remains at 100% for a bitrate of 32 kbps, but drops below 100% for bitrates below 20 kbps.

## Quantization Attack

Quantizing the audio signal by adjusting the number of bits per sample showcased a similar threshold effect. Recovery remains at 100% accuracy for 8 bits per sample but diminishes with 7 bits per sample, becoming unattainable with 6 bits per sample.

## Signal Speed Change Attack

Altering the speed of the audio signal poses challenges for recovery. Up to a 1.3x speedup or a 0.8x slowdown, recovery accuracy remains at 100%. However, beyond these thresholds, accurate recovery becomes increasingly difficult, particularly with higher speed variations.

# Conclusion

The WavMark project has demonstrated promising capabilities in embedding and recovering watermarks in audio signals, providing a valuable tool for digital forensics in the face of increasing challenges posed by deep learning-generated fake audio. Leveraging a novel approach based on an invertible Encoder/Decoder (INN), a shift module, and an attack simulator, WavMark operates in the frequency domain, ensuring higher robustness against various attacks.

The performance evaluation revealed the resilience of WavMark under different attack scenarios, emphasizing its effectiveness in maintaining the integrity of embedded messages. In the absence of any attack, the message recovery accuracy reached 100%, showcasing the robustness of the watermarking technique. Notably, the system demonstrated impressive resistance against sample suppression, low-pass filter, median filter, lossy compression, quantization, and even signal speed changes within certain thresholds.

Despite these achievements, it is essential to acknowledge the inherent challenges and limitations. Certain attack scenarios, such as higher percentages of sample suppression or extreme speed changes, proved to be more challenging for message recovery. Understanding these limitations is crucial for practitioners and investigators in real-world applications.

In conclusion, WavMark stands as a promising solution in the ongoing battle against audio tampering and forgery. As digital forensics continues to evolve, WavMark's innovative approach and robust performance position it as a valuable asset for detecting tampering in audio signals, contributing to the broader goal of ensuring the authenticity and reliability of digital evidence in an era of advanced manipulation technologies.
